


/* Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.banner');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			pagination: false,
		});

		// Custom Navigation Events
		$('.banner_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.banner_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .banner');
	}
});
/*========== Banner ==========*/









/* Comment Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.comment_bnr');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				[768, 2],
				[992, 3],
			],
			// autoHeight : true,
			navigation: false,
			// pagination: true,
			slideSpeed: 300,
			paginationSpeed: 400,
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .comment_bnr");
	}
});
/*========== Comment Banner ==========*/









/* Client Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.client_bnr');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				[768, 2],
				[992, 4],
				[992, 6],
			],
			// autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});


		// Custom Navigation Events
		$('.client_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.client_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .client_bnr");
	}
});
/*========== Client Banner ==========*/








/* Service Time
============================================================*/
$(document).on('change', '[data-time-radio]', function () {

	if ($(this).attr('data-time-radio'))
	{
		$(this).closest('.svc_form_place').find('.svc_form_place_time').addClass('active');
	}
	else
	{
		$(this).closest('.svc_form_place').find('.svc_form_place_time').removeClass('active');
	}

});
/*========== Service Time ==========*/








/* Service Upload
============================================================*/
$(document).ready(function () {

	$('.svc_form_wht_up_inp').fileinput({
		showCaption: false,
		// showRemove: false,
		showUpload: false,
		showCancel: false,
		showClose: false,
		browseLabel: 'Загрузить фото/файл',
		browseIcon: '<svg class="svc_form_wht_up_inp_i" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 612.001 612.001"' +
		'style="enable-background:new 0 0 612.001 612.001;" xml:space="preserve">' +
		'<g>' +
		'<path d="M565.488,74.616l-2.868-2.833c-63.386-61.375-164.907-60.728-227.507,1.889L45.34,363.532C23.501,385.406,0,425.134,0,460.683c0,33.38,13.027,64.802,36.65,88.407c23.641,23.658,55.08,36.686,88.53,36.686h0.018c33.45-0.018,64.89-13.045,88.513-36.702l250.151-250.168c17.188-17.188,26.596-41.004,25.756-65.379c-0.786-22.364-9.932-43.364-25.756-59.154c-16.646-16.629-38.749-25.792-62.284-25.792c-23.536,0-45.655,9.145-62.249,25.792L147.754,365.963c-4.826,4.773-7.851,11.383-7.851,18.691c0,14.479,11.733,26.229,26.229,26.229c7.239,0,13.779-2.938,18.517-7.676l0.018,0.018l191.766-191.8c6.854-6.837,16.314-10.194,25.739-10.037c9.04,0.14,18.027,3.515,24.619,10.089c6.383,6.382,10.072,14.88,10.404,23.851c0.35,10.002-3.357,19.427-10.422,26.491l-250.15,250.167c-13.744,13.744-31.999,21.315-51.425,21.333h-0.018c-19.427,0-37.699-7.589-51.443-21.333c-13.709-13.709-21.28-31.929-21.28-51.303c0-16.297,13.744-43.784,29.988-60.063l289.773-289.843c42.455-42.49,111.349-42.788,154.188-1.049l2.78,2.798c41.074,42.945,40.497,111.297-1.73,153.542L287.623,505.918c-4.809,4.773-7.799,11.349-7.799,18.657c0,14.479,11.733,26.229,26.229,26.229c7.24,0,13.761-2.938,18.518-7.658l0.017,0.018l239.975-239.991C627.51,240.188,627.807,137.967,565.488,74.616z"/>' +
		'</g>' +
		'</svg>',
		removeLabel: 'Удалить все',
		removeIcon: '<img src="img/icon/trash.png">',
		removeClass: 'btn btn-danger svc_form_wht_up_rmv',
		uploadUrl: '#',
		layoutTemplates: {
			actionDelete: '<button type="button" class="kv-file-remove svc_form_wht_up_rmv_it {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}>' +
			'<img src="img/icon/trash.png">' +
			'</button>',
			actionUpload: '',
			actionDownload: '',
			actionZoom: ''
		},
		language: 'ru',
	});

});
/*========== Service Upload ==========*/












/* Header
============================================================*/

/*========== Header ==========*/


